package main

import (
	"bufio"
	"bytes"
	"image"
	"image/color"
	"image/png"
	"io"
	"log"
	"os"
	"strings"

	"golang.org/x/ssh/term"
)

var monochromeColor = map[rune]color.NRGBA{
	'0': color.NRGBA{
		R: 255,
		G: 255,
		B: 255,
		A: 255,
	},
	'1': color.NRGBA{
		R: 0,
		G: 0,
		B: 0,
		A: 255,
	},
}

func main() {
	if term.IsTerminal(0) {
		return
	}

	// Get file name for output from argument
	fn := "image.png"
	if len(os.Args) > 1 {
		fn = os.Args[1]
	}

	r := bufio.NewReader(os.Stdin)
	b := new(bytes.Buffer)

	s, err := io.ReadAll(r)
	if err != nil {
		log.Fatal(err)
	}
	b.Write(s)

	printImage(b, fn)
}

func printImage(r io.Reader, fn string) {
	// FIXME: Maybe there is a better solution
	r1 := r
	r2 := new(bytes.Buffer)
	r3 := new(bytes.Buffer)
	r4 := new(bytes.Buffer)
	r5 := new(bytes.Buffer)
	te1 := io.TeeReader(r1, r2)
	te2 := io.TeeReader(r2, r4)
	io.Copy(r3, te1)
	io.Copy(r5, te2)

	width, err := countLineLength(r3)
	if err != nil {
		log.Fatal(err)
	}

	height, err := countLine(r4)
	if err != nil {
		log.Fatal(err)
	}

	img := image.NewNRGBA(image.Rect(0, 0, width, height))

	y := 0
	rd := bufio.NewReader(r5)
	for {
		l, err := rd.ReadString('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)

		}
		l = strings.Trim(l, "\n")

		if width != len(l) {
			log.Fatal("Invalid input")
		}

		x := 0
		for _, c := range l {
			img.Set(x, y, monochromeColor[c])
			x++
		}

		x = 0
		y++
	}

	f, err := os.Create(fn)
	if err != nil {
		log.Fatal(err)
	}

	if err := png.Encode(f, img); err != nil {
		f.Close()
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}

func countLineLength(r io.Reader) (int, error) {
	l, err := bufio.NewReader(r).ReadString('\n')
	if err == io.EOF || err != nil {
		return 0, err
	}
	l = strings.Trim(l, "\n")

	return len(l), nil
}

func countLine(r io.Reader) (int, error) {
	rd := bufio.NewReader(r)
	count := 0

	for {
		_, err := rd.ReadString('\n')

		if err == io.EOF {
			return count, nil
		}

		if err != nil {
			return count, err
		}

		count++
	}
}
